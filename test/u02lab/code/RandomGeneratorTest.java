package u02lab.code;

import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * Created by giacomo on 07/03/2017.
 */
public class RandomGeneratorTest {

    SequenceGenerator generator;
    private final static int SEQUENCE_LENGTH=8;
    private final static int NUMBER_OF_STEPS_TO_TEST=10;
    private final static int NUMBER_OF_NEXT_BEFORE_TEST=6;
    private List possibleValues;

    @org.junit.Before
    public void setUp() throws Exception {
        possibleValues = new ArrayList();
        possibleValues.add(Optional.of(1));
        possibleValues.add(Optional.of(0));
        generator = new RandomGenerator(SEQUENCE_LENGTH);
    }

    @Test
    public void next() throws Exception {
        boolean isValid=true;
        int j=0;
        Optional<Integer> value;
        for(;j<NUMBER_OF_STEPS_TO_TEST;j++){
            value=generator.next();
            if(j<SEQUENCE_LENGTH){
                if(!(value.equals(Optional.of(1))||value.equals(Optional.of(0)))){
                    isValid=false;
                }
            }else{
                if(!value.equals(Optional.empty())){
                    isValid=false;
                }
            }
        }
        Assert.assertTrue(isValid);
    }

    @Test
    public void reset() throws Exception {
        Optional<Integer> firstValue=generator.next();
        for(int i=1;i<NUMBER_OF_NEXT_BEFORE_TEST;i++){
            generator.next();
        }
        generator.reset();
        Assert.assertEquals(generator.next(),firstValue);
    }

    @org.junit.Test
    public void isOver() throws Exception {
        List list = new ArrayList();
        List results = new ArrayList();
        for (int i=0; i < NUMBER_OF_STEPS_TO_TEST; i++) {
            if(i<SEQUENCE_LENGTH)list.add(false);
            else list.add(true);
            results.add(generator.isOver());
            generator.next();
        }
        Assert.assertEquals(list,results);

    }

    @org.junit.Test
    public void allRemaining() throws Exception {
        List result;
        boolean isValid=true;
        for(int i=0;i<NUMBER_OF_NEXT_BEFORE_TEST;i++){
            generator.next();
        }
        result=generator.allRemaining();
        for (Object r:result) {
            if(!(r.equals(Optional.of(1))||r.equals(Optional.of(0)))){
                isValid=false;
            }
        }
        Assert.assertTrue(isValid);
    }
}