package u02lab.code;

import org.junit.Assert;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * Created by giacomo on 07/03/2017.
 */
public class RangeGeneratorTest {

    SequenceGenerator generator;
    private final static int START_VALUE=-2;
    private final static int STOP_VALUE=5;
    private final static int NUMBER_OF_STEPS_TO_TEST=10;
    private final static int NUMBER_OF_NEXT_BEFORE_TEST=2;

    @org.junit.Before
    public void setUp() throws Exception {
        generator = new RangeGenerator(START_VALUE,STOP_VALUE);
    }

    @org.junit.Test
    public void next() throws Exception {
        List list = new ArrayList();
        List results = new ArrayList();
        int numberOfAvailableValues=STOP_VALUE-START_VALUE;
        for(int i=0;i<NUMBER_OF_STEPS_TO_TEST; i++) {
            if(STOP_VALUE<START_VALUE||i>numberOfAvailableValues)list.add(Optional.empty());
            else list.add(Optional.of(START_VALUE + i));
            results.add(generator.next());
        }
        Assert.assertEquals(list,results);
    }
    @org.junit.Test
    public void reset() throws Exception {
        for(int i=0;i<NUMBER_OF_NEXT_BEFORE_TEST;i++){
            generator.next();
        }
        generator.reset();
        Assert.assertEquals(generator.next(),Optional.of(START_VALUE));
    }

    @org.junit.Test
    public void isOver() throws Exception {
        List list = new ArrayList();
        List results = new ArrayList();
        if(STOP_VALUE<START_VALUE){
            list.add(true);
            results.add(generator.isOver());
        }else {
            int numberOfAvailableValues=STOP_VALUE-START_VALUE;
            for (int i=0; i < NUMBER_OF_STEPS_TO_TEST; i++) {
                if(i<=numberOfAvailableValues)list.add(false);
                else list.add(true);
                results.add(generator.isOver());
                generator.next();
            }
            Assert.assertEquals(list,results);
        }
    }

    @org.junit.Test
    public void allRemaining() throws Exception {
        List expected = new ArrayList();
        List result;
            for(int i=0;i<NUMBER_OF_NEXT_BEFORE_TEST;i++){
                generator.next();
            }
            for(int i=START_VALUE;i<=STOP_VALUE;i++) {
                expected.add(Optional.of(i));
            }
        result=generator.allRemaining();
        Assert.assertEquals(expected.subList(NUMBER_OF_NEXT_BEFORE_TEST,expected.size()),result);
    }
}